import colview from "./views/colview.js";
import addView from "./views/addView.js";
import addViewSubtask from "./views/addViewSubtask.js";
import AddColModel from "./views/addColModel.js";

let j = 0;
let i = 0;
const modalWindowCol = document.querySelector(".modal-window");
const colOverlay = document.querySelector(".overlay");
console.log(colOverlay);
const createCol = document.querySelector(".create-new-column");
const addCol = document.querySelector(".add-new-col");
const saveCol = document.querySelector(".save-col");
const boardMenu = document.querySelector(".board-menu");
const editBoard = document.querySelector(".edit-board-and-overlay");
let colName = "";
const modalWindowTask = document.querySelector(".modal-window-task");
const addTask = document.querySelector(".add-task");
const taskOverlay = document.querySelector(".task-overlay");
const editBoardText = document.querySelector(".edit-board-text");
const deleteOverlay = document.querySelector(".delete-overlay");
const columnNames = [];
const addSub = document.getElementById("add-sub");
const createTask = document.getElementById("create-task");
const Columns = document.querySelector(".columns");
const editTaskModal = document.getElementById("edit-task");
//

/* Reading Data From Strapi */
let cols = await getColumns();
colview.render(cols.data);
// console.log(cols.data);
console.log(cols);
cols = cols.data.map((col) => {
  console.log(col);
  columnNames.push({ Name: col.attributes.Name, id: col.id });
});

// console.log(columnNames);

/* Deleting a Task */
Columns.addEventListener("click", () => {
  console.log("hilo");
  // editTaskModal.classList.toggle("hidden");
});

/* Deleting a Column */
boardMenu.addEventListener("click", () => {
  editBoard.classList.toggle("hidden");
});
deleteOverlay.addEventListener("click", () => {
  editBoard.classList.toggle("hidden");
});
// console.log(columnNames);
/* Edit Board */
editBoardText.addEventListener("click", () => {
  editBoard.classList.toggle("hidden");
  modalWindowCol.classList.toggle("hidden");
  new AddColModel("Edit Board").render(columnNames);
});
// modalWindowCol.querySelector(".close-col").addEventListener("click", () => {
//   modalWindowCol.classList.toggle("hidden");
// });
// modalWindowCol.querySelector(".overlay").addEventListener("click", () => {
//   modalWindowCol.classList.toggle("hidden");
// });

/* Adding a New Task */
addTask.addEventListener("click", () => {
  document.getElementById(
    "subtasks-add-task"
  ).innerHTML = `<input type="text" class="subtasks-add-task-content" />`;
  modalWindowTask.classList.toggle("hidden");
  // console.log(columnNames);
  const select = document.querySelector(".status-select-parent");

  const Markup = `<select class="status-select"  name="" id="status-select">
  ${columnNames.map((col) => {
    return ` <option value="${col.id}">${col.Name}</option>`;
  })}
</select>`;
  // console.log(Markup);
  select.innerHTML = "";
  select.insertAdjacentHTML("beforeend", Markup);
});
taskOverlay.addEventListener("click", (e) => {
  e.preventDefault();
  e.stopImmediatePropagation();
  modalWindowTask.classList.toggle("hidden");
  j = 0;
});

modalWindowTask.querySelector(".close-task").addEventListener("click", () => {
  modalWindowTask.classList.toggle("hidden");
  j = 0;
});

createTask.addEventListener("click", async () => {
  let newTaskInput = document.getElementById("task-name");
  let status = document.getElementById("status-select").value;
  // console.log(status);

  // console.log("hello");
  const response = await fetch(`http://localhost:1337/api/tasks`, {
    method: "post",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      data: {
        Name: `${newTaskInput.value}`,
        column: `${status}`,
      },
    }),
  });
  const data = await response.json();
  // newColInput.classList.remove("new-col-input");
  modalWindowTask.classList.toggle("hidden");
  // j = 0;

  location.reload();
});

/* Working on adding multiple subtasks within the new task */
j = 0;
addSub.addEventListener("click", () => {
  addViewSubtask.render(j);
  j++;
  // console.log(j);
});

/* Adding a New Column */
createCol.addEventListener("click", () => {
  modalWindowCol.classList.toggle("hidden");
  const addNewCol = new AddColModel("Add new Column");
  console.log(columnNames);
  addNewCol.render(columnNames);
  i = 0;
});

colOverlay.addEventListener("click", (e) => {
  e.preventDefault();
  e.stopImmediatePropagation();
  // console.log("clicked");
  modalWindowCol.classList.toggle("hidden");
  i = 0;
});
modalWindowCol.querySelector(".close-col").addEventListener("click", () => {
  modalWindowCol.classList.toggle("hidden");
  i = 0;
});

addCol.addEventListener("click", () => {
  addView.render(i);
  i++;
});

saveCol.addEventListener("click", async () => {
  let newColInput = document.querySelector(".new-col-input");
  const response = await fetch(`http://localhost:1337/api/columns`, {
    method: "post",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      data: {
        Name: `${newColInput.value}`,
      },
    }),
  });
  const data = await response.json();
  // newColInput.classList.remove("new-col-input");
  modalWindowCol.classList.add("hidden");
  i = 0;
  location.reload();
  return data;
});
//

//

/* Reading Data From Strapi*/

async function getColumns() {
  const response = await fetch(
    "http://localhost:1337/api/columns?populate=deep"
  );
  const data = await response.json();
  return data;
}

// console.log(columnNames);

// async function getSubtasks() {
//   const response = await fetch(
//     "http://localhost:1337/api/columns?populate=deep"
//   );
//   const data = await response.json();
//   return data.data;
// }
// console.log(await getSubtasks());

//
// DRAG AND DROP + UPDATING STRAPI
//
const cards = document.querySelectorAll(".card");
const columns = document.querySelectorAll(".column");
let colid;
let id;

cards.forEach((card) => {
  card.addEventListener("dragstart", () => {
    // console.log("we r draggin");
    card.classList.add("dragging");
  });
  card.addEventListener("dragend", async () => {
    console.log("we finished The draggin Operation");
    id = card.id;
    console.log(`Task id: ${id}`);
    console.log(`Column id: ${colid}`);
    updateTask(id, colid);
    card.classList.remove("dragging");
  });
});

columns.forEach((column) => {
  column.addEventListener("dragover", (e) => {
    colid = column.id;
    // console.log(colid);
    const card = document.querySelector(".dragging");
    e.preventDefault();
    const afterElement = drag(column, e.clientY);
    // console.log(afterElement);
    if (afterElement == null) {
      column.appendChild(card);
    } else {
      column.insertBefore(card, afterElement);
    }
  });
});

function drag(column, y) {
  const draggableElements = [
    ...column.querySelectorAll(".card:not(.dragging)"),
  ];
  return draggableElements.reduce(
    (closest, child) => {
      const box = child.getBoundingClientRect();
      const difference = y - box.top - box.height / 2;
      // console.log(difference);
      if (difference < 0 && difference > closest.difference) {
        return { difference: difference, element: child };
      } else {
        return closest;
      }
    },
    { difference: Number.NEGATIVE_INFINITY }
  ).element;
}
/* Update Strapi's Data Based on the Drag and Drop */
async function updateTask(id, colid) {
  const response = await fetch(
    `http://localhost:1337/api/tasks/${id}?populate=*`,
    {
      method: "put",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        data: {
          column: {
            connect: [`${colid}`],
          },
        },
      }),
    }
  );
  const data = await response.json();
  return data;
}
