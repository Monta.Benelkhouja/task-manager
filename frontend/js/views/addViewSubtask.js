//

class addviewsubtask {
  parentEl = document.getElementById("subtasks-add-task");
  render(i) {
    // this.data = data;
    console.log(i);
    const html = this.generateMarkup(i);
    console.log(this.parentEl);
    this.parentEl.insertAdjacentHTML("beforeend", html);
  }
  generateMarkup(i) {
    if (i > 2) {
      return "";
    } else {
      return `<input type="text" class="subtasks-add-task-content" />`;
    }
  }
}
export default new addviewsubtask();
