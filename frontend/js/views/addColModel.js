export default class AddColModel {
  constructor(title) {
    // console.log(title);
    this.parentEl = document.getElementById(`columns-inputs`);
    document.getElementById("col-modal-add-new-col-title").textContent = title;
  }

  render(data) {
    this.data = data;
    const html = this.generateMarkup();
    // console.log(html);
    this.parentEl.innerHTML = "";
    this.parentEl.insertAdjacentHTML("beforeend", html);
    this.handleDelCol();
  }
  generateMarkup() {
    // console.log(this.data);
    return this.data
      .map(
        (col) => `<div class="space">
        <input type="text" value="${col.Name}" class="col-input" />
        <svg
                class="del-col"
                id="col${col.id}"
                width="15"
                height="15"
                xmlns="http://www.w3.org/2000/svg"
              >
                <g fill="#828FA3" fill-rule="evenodd">
                  <path d="m12.728 0 2.122 2.122L2.122 14.85 0 12.728z"></path>
                  <path d="M0 2.122 2.122 0 14.85 12.728l-2.122 2.122z"></path>
                </g>
              </svg></div>`
      )
      .join(" ");
  }

  handleDelCol() {
    this.data.forEach((col) => {
      const delCol = document.getElementById(`col${col.id}`);
      delCol.addEventListener("click", async (e) => {
        const space = e.target.closest(".space");
        if (!space) return;
        space.remove();
        const response = await fetch(
          `http://localhost:1337/api/columns/${col.id}`,
          {
            method: "delete",
          }
        );
        const data = await response.json();
        location.reload();
      });
    });
  }
}
