class colview {
  parentEl = document.querySelector(".columns");
  render(data) {
    let i = 0;
    this.data = data;
    const html = this.generateMarkup(i);
    // console.log(data[0].attributes.Name);

    // console.log(data);
    // console.log(this.generateMarkup());
    // console.log(html);
    this.parentEl.innerHTML = "";
    this.parentEl.insertAdjacentHTML(`beforeend`, html);
  }
  generateMarkup(i) {
    return this.data
      .map((col) => {
        i++;
        // console.log(col.attributes.Name);
        return `
      <div class="column" id=${col.id}>
      <div class="ballandtitle">
        <span class="title-ball title-ball${i}"></span>
        <p class="column-title">${col.attributes.Name}</p>
      </div>
      ${col.attributes.tasks.data
        .map((task) => {
          return `<div class="card" id=${task.id} draggable="true">
                    <p class="card-title">${task.attributes.Name}</p>
                   <p class="card-count">0 of ${task.attributes.subtasks.data.length} 
                   subtasks</p> 
                  </div>`;
        })
        .join(" ")}
       
      </div>`;
      })
      .join(" ");
  }
}

export default new colview();
