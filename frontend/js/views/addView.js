//

class addview {
  parentEl = document.querySelector(".columns-inputs");
  render(i) {
    // this.data = data;
    console.log(i);
    const html = this.generateMarkup(i);
    this.parentEl.insertAdjacentHTML("beforeend", html);
  }
  generateMarkup(i) {
    if (i > 0) return;

    return `<input type="text" placeholder="Enter a new column" class="col-input new-col-input" />`;
  }
}
export default new addview();
