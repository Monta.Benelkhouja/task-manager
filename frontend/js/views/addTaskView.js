//

class addTaskView {
  parentEl = document.querySelector(".modal-window-task");
  render() {
    // this.data = data;
    // console.log();
    const html = this.generateMarkup();
    this.parentEl.insertAdjacentHTML("beforeend", html);
  }
  generateMarkup() {
    return `<div class="task-overlay"></div>
      <div class="modal">
        <div class="modal-title">
          <p class="add-new-col-title">Add New Task</p>
          <svg
            class="close-task"
            width="15"
            height="15"
            xmlns="http://www.w3.org/2000/svg"
          >
            <g fill="#828FA3" fill-rule="evenodd">
              <path d="m12.728 0 2.122 2.122L2.122 14.85 0 12.728z"></path>
              <path d="M0 2.122 2.122 0 14.85 12.728l-2.122 2.122z"></path>
            </g>
          </svg>
        </div>

        <div class="name">
          <p class="col-name">Title</p>
          <input type="text" placeholder="" class="add-task-name-input" />
        </div>
        <div class="Description">
          <p class="Description-title">Description</p>
          <textarea
            class="description-textarea"
            name=""
            id=""
            cols="40"
            rows="10"
          ></textarea>
        </div>
        <div class="subtasks-add-task">
          <p>Subtasks</p>
          <input type="text" class="subtasks-add-task-content" />
        </div>
        <div class="add-columns"></div>
        <div class="add-btns">
          <button class="create-btn white-btn">+Add New Subtask</button>
          <label for="status">Status</label>
          <select class="status-select" name="" id="">
            <option value="first-col">To do</option>
            <option value="second-col">Done</option>
            <option value="third-col">Doing</option>
          </select>

          <button class="create-btn">Create-Task</button>
        </div>
    </div>`;
  }
}
export default new addTaskView();
